class CalculatorController < ActionController::Base
    def home
        @n1 = params[:n1].to_i
        @n2 = params[:n2].to_i
    end

    def result
        n1 = params[:n1].to_f
        n2 = params[:n2].to_f

        case params[:op]
        when "sum"
            @result = n1 + n2
        when "sub"
            @result = n1 - n2
        when "mul"
            @result = n1 * n2
        when "div"
            @result = n1 / n2
        end
    end
end