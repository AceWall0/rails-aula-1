Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get '/calculadora/:n1/:n2',     to: 'calculator#home'
  get '/calculadora/:n1/:n2/:op', to: 'calculator#result', as: 'calc_res'
end
